# NBA Heights

This project with an input value finds two players whose height sum in inches is exactly the input entered.

## Installation

To run the project we must first install all dependencies using
### `npm install`

## Run

To run the project we must execute the following command
### ` node app.js 139 `
As argument we pass the value we want, for example 139 or any value.

### Autor: Estefany Muriel Cano