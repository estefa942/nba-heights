export function quickSort([x = [], ...xs], attribute) {
    return (x.length === 0) ? [] : [
        ...quickSort(xs.filter(y => y[attribute] <= x[attribute]), attribute),
        x,
        ...quickSort(xs.filter(y => y[attribute] > x[attribute]), attribute)
    ];
}

