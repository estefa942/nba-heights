export function binarySearch(arr, value, start, end, attribute) {
    if (start > end) return -1;
    const mid = Math.floor((start + end) / 2);
    if (arr[mid][attribute] == value) return arr[mid];
    if (arr[mid][attribute] > value)
        return binarySearch(arr, value, start, mid - 1, attribute);
    else
        return binarySearch(arr, value, mid + 1, end, attribute);
}
