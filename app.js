import { got } from 'got';
import { quickSort } from './sort.js';
import { binarySearch } from './search.js'

async function main() {
    const args = process.argv.slice(2)
    const INPUT = args[0];
    const URL = 'https://mach-eight.uc.r.appspot.com/';
    const { values: players } = await got(URL).json();
    getPlayerPairsByInput(players, INPUT);
}

function getPlayerPairsByInput(players, input){
    const pairs = [];
    const searchAtttribute = 'h_in'
    const sortedPlayers = quickSort(players, searchAtttribute) 
    for (const player of sortedPlayers) {
        const selectedPlayer = binarySearchHeights(sortedPlayers, input - parseInt(player.h_in), searchAtttribute) 
        const firstPlayerName = `${player.first_name} ${player.last_name}`;
        const selectedPlayerName = `${selectedPlayer.first_name} ${selectedPlayer.last_name}`; 
        if(selectedPlayer != -1 && firstPlayerName != selectedPlayerName) { 
            const auxArray = [firstPlayerName, selectedPlayerName].sort() 
            const result = auxArray.join('\t'); 
            pairs.push(result); 
        }
    }
    showPlayerPairs(pairs);
}

function showPlayerPairs(pairs){
    if(pairs.length > 0) {
        const uniquePairs = [...new Set(pairs)]; 
        console.log(uniquePairs.join('\n')); 
    } else {
        console.log('No matches found');
    }
}

function binarySearchHeights(array, desiredHeight, attribute) {
    return binarySearch(array, desiredHeight, 0, array.length, attribute)
}

main()